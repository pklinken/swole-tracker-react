export enum LogSeverity {
  INFO = 1,
  DEBUG,
  ERROR,
}

export function debugLog(msg: any): void {
  log(msg, LogSeverity.DEBUG);
}

export function log(msg: any, severity = LogSeverity.INFO): void {
  if (process.env.NODE_ENV === "production" && severity === LogSeverity.DEBUG)
    return;

  switch (severity) {
    case LogSeverity.DEBUG:
      console.debug(msg);
      break;
    case LogSeverity.INFO:
      console.info(msg);
      break;
    case LogSeverity.ERROR:
      console.error(msg);
      break;
    default:
      console.log(msg);
  }
}

export const formattedDateHelper = function (date: number): string {
  return new Date(date).toLocaleDateString();
};

export const groupBy = function <TItem>(
  xs: TItem[],
  key: string
): { [key: string]: TItem[] } {
  return xs.reduce(function (rv: { [key: string]: TItem[] }, x: any) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

import { storeService } from "./store";
// import { InExerciseDataPoint } from '@/models/ExerciseDatapoint';
import { ISearchResult } from "models/SearchResult";
import { debugLog } from "helpers/Utility";

class SearchService {
  search(searchTerm: string): Promise<Array<ISearchResult>> {
    return new Promise((resolve, _reject) => {
      debugLog(`Searching for ${searchTerm}`);
      const returnValue: Array<ISearchResult> = [];

      const matchingExercises = storeService.searchExercisesByName(searchTerm);
      matchingExercises.forEach((ex) => {
        returnValue.push({
          exercise: ex,
          datapoints: storeService.getDataPointsForId(ex.id),
        });
      });
      resolve(returnValue);
    });
  }
}

export const searchService = new SearchService();

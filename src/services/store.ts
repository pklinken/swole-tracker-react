import { IExerciseDataPoint } from "models/ExerciseDatapoint";
import { IExercise, Exercise } from "models/Exercise";
import IResult, { Err, Ok } from "models/Result";

export const datapointStorageKey = "swole-tracker-datapoints";
export const exerciseStorageKey = "swole-tracker-exercises";

// New interface that uses promises so we can exchange it with an online storage later

// interface Store {
//   createExercise(name: string): Promise<IExercise>;
//   createDatapoint(datapoint: IExerciseDataPoint): Promise<IExerciseDataPoint>;
//   getExercises(): Promise<IExercise[]>;
//   getExercise(id: number & string): Promise<IExercise>;
//   getExerciseNyName(name: string): Promise<IExercise>;
//   getDatapoints(exerciseId: number & string): Promise<IExerciseDataPoint[]>
//   updateExercise(exercise: IExercise): Promise<IResult<object>>

// }

class StoreService {
  createDatapoint(datapoint: IExerciseDataPoint) {
    const currentData: Array<IExerciseDataPoint> = this.getDatapoints();
    // This doesn't check if exercise already exists
    datapoint.id = this.nextDatapointId();
    currentData.push(datapoint);
    // One-time conversion to get rid of numeric strings in my data
    // currentData = currentData.map((dp) => {
    //   dp.exerciseId = parseInt(dp.exerciseId.toString());
    //   return dp;
    // });
    localStorage.setItem(datapointStorageKey, JSON.stringify(currentData));
  }

  getDatapoint(id: number): IExerciseDataPoint | undefined {
    return this.getDatapoints().find((dp) => dp.id === id);
  }

  updateDatapoint(datapoint: IExerciseDataPoint) {
    const currentData: Array<IExerciseDataPoint> = this.getDatapoints();
    let dpToUpdate = currentData.find((dp) => dp.id === datapoint.id);
    if (!dpToUpdate) return;

    datapoint.exerciseId = dpToUpdate.exerciseId;
    // dpToUpdate = datapoint;
    Object.assign(dpToUpdate, datapoint);
    localStorage.setItem(datapointStorageKey, JSON.stringify(currentData));
  }

  deleteDatapoint(id: number) {
    let currentData: Array<IExerciseDataPoint> = this.getDatapoints();

    currentData = currentData.filter((dp) => dp.id !== id);
    localStorage.setItem(datapointStorageKey, JSON.stringify(currentData));
  }

  getDatapoints(): Array<IExerciseDataPoint> {
    const currentDataString = localStorage.getItem(datapointStorageKey);
    return currentDataString !== null ? JSON.parse(currentDataString) : [];
  }

  getDataPointsForId(id: any): Array<IExerciseDataPoint> {
    const exerciseId = parseInt(id);
    if (isNaN(exerciseId)) return [];

    return this.getDatapoints().filter((dp) => dp.exerciseId === exerciseId);
  }

  createExercise(name: string): IExercise | null {
    if (!this.getExerciseByName(name)) {
      // If exercise does not already exist
      const exercises = this.getExercises();
      const createdExercise = new Exercise(this.nextExerciseId(), name);
      exercises.push(createdExercise);
      localStorage.setItem(exerciseStorageKey, JSON.stringify(exercises));
      return createdExercise;
    } else return null;
  }

  searchExercisesByName(name: string): Array<IExercise> {
    return this.getExercises().filter((e) =>
      e.name.toLowerCase().includes(name.toLowerCase())
    );
  }

  getExercise(id: any): IExercise | undefined {
    const exerciseId = parseInt(id);
    if (isNaN(exerciseId)) return undefined;

    return this.getExercises().find((e) => e.id === exerciseId);
  }

  updateExercise(exercise: IExercise): IResult<object> {
    const exercises = this.getExercises();
    let exerciseToUpdate = exercises.find((e) => e.id === exercise.id);

    if (exerciseToUpdate) {
      if (this.getExerciseByName(exercise.name)) {
        // Other exercise with same name already exists
        return Err(`Exercise with name "${exercise.name} already exists.`);
      } else {
        exerciseToUpdate.name = exercise.name;
        localStorage.setItem(exerciseStorageKey, JSON.stringify(exercises));
        return Ok({});
      }
    } else {
      return Err(`Exercise with ID ${exercise.id} not found.`);
    }
  }

  getExerciseByName(name: string): IExercise | undefined {
    return this.getExercises().find((e) => e.name === name);
  }

  getExercises(): Array<IExercise> {
    const currentDataString = localStorage.getItem(exerciseStorageKey);
    return currentDataString !== null ? JSON.parse(currentDataString) : [];
  }

  private nextExerciseId(): number {
    const exercises = this.getExercises();
    let highestId = 1;
    exercises.forEach((ex) => {
      if (ex.id > highestId) highestId = ex.id;
    });
    return highestId + 1;
  }

  private nextDatapointId(): number {
    const datapoints = this.getDatapoints();
    let highestId = 1;
    datapoints.forEach((dp) => {
      if (dp.id > highestId) highestId = dp.id;
    });
    return highestId + 1;
  }
}

export const storeService = new StoreService();

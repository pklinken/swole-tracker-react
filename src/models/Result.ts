export enum ResultType {
  Ok,
  Error,
}

export default interface IResult<T> {
  type: ResultType;
  msg?: string;
  value?: T;
}

export function Ok<T>(value: T): IResult<T> {
  return {
    type: ResultType.Ok,
    value: value,
  };
}

export function Err<T>(msg: string): IResult<T> {
  return {
    type: ResultType.Error,
    msg: msg,
  };
}

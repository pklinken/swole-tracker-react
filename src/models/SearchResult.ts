import { IExerciseDataPoint } from "./ExerciseDatapoint";
import { IExercise } from "./Exercise";

export interface ISearchResult extends Object {
  exercise: IExercise;
  datapoints: Array<IExerciseDataPoint>;
}

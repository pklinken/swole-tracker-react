export interface IExercise {
  id: number;
  name: string;
}

export class Exercise implements IExercise {
  id: number;
  name: string;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}

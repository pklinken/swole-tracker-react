import { formattedDateHelper, groupBy } from "helpers/Utility";

export interface IExerciseDataPoint {
  id: number;
  date: number;
  exerciseId: number;
  weight: number;
  reps: number;
  note: string;
}

export class ExerciseDatapoint implements IExerciseDataPoint {
  id: number;
  date: number;
  exerciseId: number;
  weight: number;
  reps: number;
  note: string;

  constructor(
    id = -1,
    date = Date.now(),
    exerciseId = -1,
    weight = 0,
    reps = 0,
    note = ""
  ) {
    this.id = id;
    this.date = date;
    this.exerciseId = exerciseId;
    this.weight = weight;
    this.reps = reps;
    this.note = note;
  }
}

export function getMostRecentSessions(
  datapoints: IExerciseDataPoint[]
): Array<IExerciseDataPoint> {
  return (
    datapoints
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .map((dp: any) => {
        return dp;
      })
      .sort((a: IExerciseDataPoint, b: IExerciseDataPoint) => b.date - a.date)
  );
}

export function getMostRecentSessionDate(
  datapoints: IExerciseDataPoint[]
): string {
  return formattedDateHelper(Math.max(...datapoints.map((dp) => dp.date)));
}

export function getSessionsByDate(
  formattedDate: string,
  datapoints: IExerciseDataPoint[]
): IExerciseDataPoint[] {
  const descendingDateCompare = (
    a: IExerciseDataPoint,
    b: IExerciseDataPoint
  ) => {
    return b.date - a.date;
  };

  const setsSortedByDate = datapoints.slice().sort(descendingDateCompare);

  const withFormattedDate = setsSortedByDate.map((dp) => {
    return {
      ...dp,
      formattedDate: formattedDateHelper(dp.date),
    };
  });

  return groupBy(withFormattedDate, "formattedDate")[formattedDate];
}

export function getPr(datapoints: IExerciseDataPoint[]): number {
  return datapoints.length > 0 ? Math.max(...datapoints.map((dp) => dp.weight)) : 0;
}

import "bootstrap/dist/css/bootstrap.min.css";

// To be considered later: https://github.com/styled-components/styled-components
import "./App.scss";

import React from "react";
// import logo from './logo.svg';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Search from "components/Search";
import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink,
  Switch,
} from "react-router-dom";
import Exercise from "components/Exercise";
import ExerciseList from "components/ExerciseList";
import Toasts from "components/Toasts";
import DataPointEdit from "components/DataPointEdit";

function App() {
  return (
    <div className="App">
      <Router
        basename={
          process.env.NODE_ENV === "production" ? "/swole-tracker-react" : ""
        }
      >
        <Navbar bg="light" expand="lg" collapseOnSelect={true} >
          <Link className="navbar-brand" to="/">
            SWOLE-TRACKER
          </Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link eventKey="1">
                <NavLink to="/" className="nav-link" exact>
                  Search
                </NavLink>
              </Nav.Link>
              <Nav.Link eventKey="2">
                <NavLink to="/exercises" className="nav-link" exact>
                  Exercises
                </NavLink>
              </Nav.Link>
              <Nav.Link eventKey="3">
                <NavLink to="/about" className="nav-link">
                  About
                </NavLink>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <Switch>
          <Route path="/" exact>
            <Search></Search>
          </Route>
          <Route path="/about">This is the content for the about page</Route>
          <Route path="/exercise/:id">
            <Exercise></Exercise>
          </Route>
          <Route path="/exercises/">
            <ExerciseList />
          </Route>
          <Route path="/datapoint/:id">
            <DataPointEdit />
          </Route>
        </Switch>
      </Router>
      <Toasts />
    </div>
  );
}

export default App;

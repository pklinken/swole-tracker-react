import React from "react";
import Toast from "react-bootstrap/Toast";

interface StandardToastProps {
  header?: string;
  message?: string;
  timeout?: number;
  onDismiss: Function;
  show: boolean;
}

function StandardToast(props: StandardToastProps) {
  return (
    <div>
      <Toast
        style={{ margin: "0 auto", maxWidth: "240px" }}
        show={props.show}
        delay={props.timeout || 2000}
        autohide
        onClose={() => props.onDismiss(false)}
        animation={false} // Using Fade animation give console error in strict mode
      >
        {props.header && (
          <Toast.Header>
            <strong className="mr-auto">{props.header}</strong>
          </Toast.Header>
        )}
        {props.message && <Toast.Body>{props.message}</Toast.Body>}
      </Toast>
    </div>
  );
}

export default StandardToast;

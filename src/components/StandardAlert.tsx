import React, { ClassAttributes } from "react";

import Alert from "react-bootstrap/esm/Alert";

interface IStandardAlertProps extends ClassAttributes<Alert> {
  header?: string;
  message?: string;
  show: boolean;
  onDismiss: Function;
}

function StandardAlert(props: React.PropsWithChildren<IStandardAlertProps>) {
  return (
    <>
      {props.show && (
        <Alert
          variant="danger"
          onClose={() => props.onDismiss(false)}
          dismissible
        >
          {props.header && <Alert.Heading>{props.header}</Alert.Heading>}
          {props.message && <p>{props.message}</p>}
          {props.children}
        </Alert>
      )}
    </>
  );
}

export default StandardAlert;

import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { storeService } from "services/store";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";
import { IExercise } from "models/Exercise";
import StandardAlert from "./StandardAlert";
import DataPointForm from "./DataPointForm";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { ToastBus } from "./Toasts";
import { ResultType } from "models/Result";
import LastSessionTable from "./LastSessionTable";

function Exercise() {
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [exercise, setExercise] = useState(undefined as IExercise | undefined);

  const [editing, setEditing] = useState(false);
  const [newName, setNewName] = useState("");

  const [showAlert, setShowAlert] = useState(false);

  useEffect(updateHandler, [id]);

  // function getPr(): string {
  //   let pr;
  //   const dps = storeService.getDataPointsForId(id);
  //   if (dps.length > 0)
  //     pr = Math.max(...dps.map((dp) => dp.weight));

  //   return pr ? `${pr} Kg` : "No data recorded!";
  // }

  function updateHandler() {
    const exercise = storeService.getExercise(id);
    setLoading(false);

    if (exercise === undefined) {
      setShowAlert(true);
      return;
    }

    setExercise(exercise);
  }

  function handleEditBegin() {
    setNewName(exercise ? exercise.name : "");
    setEditing(true);
  }

  function handleKeyUp(event: any) {
    switch (event.key) {
      case "Enter":
        handleEditEnd();
        break;
      case "Escape":
        setEditing(false);
        break;
    }
  }

  function handleEditEnd() {
    if (exercise && exercise.name !== newName && newName.trim().length !== 0) {
      const result = storeService.updateExercise({
        id: exercise.id,
        name: newName,
      });

      if (result.type === ResultType.Error) {
        ToastBus.publishToast({
          header: "Error updating exercise",
          message: result.msg || "Unknown error",
        });
      } else {
        ToastBus.publishToast({ header: "Exercise updated" });
      }
    }

    setEditing(false);
    updateHandler();
  }

  return (
    <Container fluid className="mt-2">
      <StandardAlert
        header="Oops"
        message="Exercise not found, was it removed ?"
        show={showAlert}
        onDismiss={setShowAlert}
      >
        <Link to="/">Back to frontpage</Link>
      </StandardAlert>

      {loading && (
        <div className="text-center">
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
      )}
      {exercise && (
        <div className="exercise">
          <div className="name d-flex justify-content-between align-items-center mb-3">
            {editing ? (
              <Form.Group>
                <Form.Control
                  className=""
                  type="text"
                  placeholder={exercise.name}
                  value={newName}
                  name="name"
                  onChange={(event) => setNewName(event.target.value)}
                  onBlur={handleEditEnd}
                  onKeyUp={handleKeyUp}
                  autoFocus
                />
              </Form.Group>
            ) : (
              <h4>{exercise.name}</h4>
            )}

            <Button variant="light" onClick={handleEditBegin}>
              <svg
                width="1em"
                height="1em"
                viewBox="0 0 16 16"
                className="bi bi-pencil"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"
                />
              </svg>
            </Button>
          </div>
          <LastSessionTable
            datapoints={storeService.getDataPointsForId(exercise.id)}
          />
          <DataPointForm exerciseId={exercise.id} onUpdate={updateHandler} />
        </div>
      )}
    </Container>
  );
}

export default Exercise;

import React, { useState, useEffect } from "react";
import SearchResults from "./SearchResults";
import { searchService } from "services/search";
import { ISearchResult } from "models/SearchResult";

function ExerciseList() {
  const [searchResults, setSearchResults] = useState([] as ISearchResult[]);

  useEffect(() => {
    searchService.search("").then((res) => setSearchResults(res));
  }, []);

  return (
    <SearchResults searchResults={searchResults}></SearchResults>
  );
}

export default ExerciseList;

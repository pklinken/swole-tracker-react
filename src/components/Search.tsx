import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import { searchService } from "services/search";
import { ISearchResult } from "models/SearchResult";
import SearchResults from "./SearchResults";
import Button from "react-bootstrap/Button";
import { storeService } from "services/store";
import { useHistory } from "react-router-dom";
import StandardAlert from "./StandardAlert";

function Search(props: any) {
  const [searchResults, setSearchResults] = useState([] as ISearchResult[]);
  const [searchFieldValue, setSearchFieldValue] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const history = useHistory();

  function isSearchFieldBlank(): boolean {
    return searchFieldValue.length === 0;
  }

  function handleInput(e: any) {
    const inputVal = e.target.value;
    if (inputVal.length > 0) {
      searchService
        .search(e.target.value)
        .then((res: ISearchResult[]) => setSearchResults(res));
    }
    setSearchFieldValue(inputVal);
  }

  function handleSubmit() {
    if(searchResults.length > 0) {
      history.push(`/exercise/${searchResults[0].exercise.id}`);
    } else {
      handleCreateExercise();
    }
  }

  function handleCreateExercise() {
    if (isSearchFieldBlank()) return;

    const newExercise = storeService.createExercise(searchFieldValue);

    if (!newExercise) {
      setShowAlert(true);
    } else {
      history.push(`/exercise/${newExercise.id}`);
    }
  }

  return (
    <Container fluid className="search mt-2">
      <StandardAlert
        header="Guru meditation error #2918"
        message="An error occurred trying to create a new exercise. Please try again later."
        show={showAlert}
        onDismiss={setShowAlert}
      />
      <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Form.Control
            type="text"
            placeholder="Search exercise"
            onInput={handleInput}
            
            autoFocus
          />
        </Form.Group>
      </Form>
      {!isSearchFieldBlank() && (
        <>
          <SearchResults searchResults={searchResults} />
          <Button variant="link" className="mt-2" onClick={handleCreateExercise}>
            Create new exercise: {searchFieldValue}
          </Button>
        </>
      )}
    </Container>
  );
}

export default Search;

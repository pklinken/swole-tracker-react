import React from "react";
import { ISearchResult } from "models/SearchResult";
import SearchResultItem from "./SearchResultItem";
import ListGroup from "react-bootstrap/ListGroup";

interface SearchResultProps {
  searchResults: ISearchResult[];
}

function SearchResults(props: SearchResultProps) {
  return (
    <ListGroup>
      {props.searchResults.map((row: ISearchResult) => (
        <SearchResultItem searchResult={row} key={row.exercise.id} />
      ))}
    </ListGroup>
  );
}

export default SearchResults;

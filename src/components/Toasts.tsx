import React, { useState, useEffect } from "react";
import StandardToast from "./StandardToast";

export interface Toast {
  header: string;
  message?: string;
}

interface PublishedToast {
  id: number;
  toast: Toast;
}

export const ToastBus = (function () {
  let registeredAddHandlers: Function[] = [];
  let registeredRemoveHandlers: Function[] = [];
  let nextId = 0;

  return {
    subscribeAddToast(handler: Function) {
      registeredAddHandlers = registeredAddHandlers.concat(handler);
    },
    subscribeRemoveToast(handler: Function) {
      registeredRemoveHandlers = registeredRemoveHandlers.concat(handler);
    },
    unsubscribeAddToast(handler: Function) {
      registeredAddHandlers = registeredAddHandlers.filter(
        (h) => h !== handler
      );
    },
    unsubscribeRemoveToast(handler: Function) {
      registeredRemoveHandlers = registeredRemoveHandlers.filter(
        (h) => h !== handler
      );
    },
    publishToast(toast: Toast) {
      const pt = {
        id: nextId++,
        toast: toast,
      };
      registeredAddHandlers.forEach((h) => h(pt));
    },
    unPublishToast(id: number) {
      registeredRemoveHandlers.forEach((h) => h(id));
    },
  };
})();

function useToasts(): PublishedToast[] {
  const [toasts, setToasts] = useState([] as PublishedToast[]);

  function handleAddToast(toast: PublishedToast) {
    setToasts(toasts.concat(toast));
  }

  function handleRemoveToast(id: number) {
    setToasts(toasts.filter((t) => t.id !== id));
  }

  useEffect(() => {
    ToastBus.subscribeAddToast(handleAddToast);
    ToastBus.subscribeRemoveToast(handleRemoveToast);
    return () => {
      ToastBus.unsubscribeAddToast(handleAddToast);
      ToastBus.unsubscribeRemoveToast(handleRemoveToast);
    };
  });

  return toasts;
}

function Toasts() {
  const toasts: PublishedToast[] = useToasts();
  return (
    <div
      style={{
        position: "fixed",
        bottom: "64px",
        width: "calc(100% - 2rem)",
      }}
    >
      {toasts.map((pt) => (
        <StandardToast
          key={pt.id}
          header={pt.toast.header}
          message={pt.toast.message}
          show={true}
          onDismiss={() => {
            ToastBus.unPublishToast(pt.id);
          }}
        />
      ))}
    </div>
  );
}

export default Toasts;

import React from "react";
import { ISearchResult } from "models/SearchResult";
import { useHistory } from "react-router-dom";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import { getMostRecentSessionDate, getPr } from "models/ExerciseDatapoint";

interface SearchResultItemProps {
  searchResult: ISearchResult;
}

function SearchResultItem(props: SearchResultItemProps) {
  const history = useHistory();

  function hasData() {
    return props.searchResult.datapoints.length > 0;
  }

  function handleClick() {
    history.push(`/exercise/${props.searchResult.exercise.id}`);
  }

  return (
    <ListGroupItem
      className="d-flex justify-content-between searchResultItem"
      onClick={handleClick}
      action
    >
      <h5 className="align-self-center">{props.searchResult.exercise.name}</h5>
      <div className="d-flex flex-column align-self-center text-right text-muted">
        {hasData() && (
          <>
            <span>Last session: {getMostRecentSessionDate(props.searchResult.datapoints)}</span>
            <span>PR: {getPr(props.searchResult.datapoints)} kg</span>
          </>
        )}
      </div>
    </ListGroupItem>
  );
}

export default SearchResultItem;

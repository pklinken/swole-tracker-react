import React, { useState, useEffect } from "react";
import {
  IExerciseDataPoint,
  getMostRecentSessionDate,
  getSessionsByDate,
} from "models/ExerciseDatapoint";

interface Props {
  datapoints: IExerciseDataPoint[];
}

function LastSessionSummary(props: Props) {
  const [sessionDate, setSessionDate] = useState("");
  const [sessionSummary, setSessionSummary] = useState("-");

  useEffect(() => {
    if (props.datapoints.length === 0) return;

    const mostRecentSessionDate = getMostRecentSessionDate(props.datapoints);
    const mostRecentSessionSets = getSessionsByDate(
      mostRecentSessionDate,
      props.datapoints
    );

    setSessionDate(mostRecentSessionDate);
    setSessionSummary(
      mostRecentSessionSets.reverse().reduce((acc, curr) => {
        return acc + `${curr.reps}x${curr.weight}kg `;
      }, "")
    );
  }, [props]);

  return (
    <span className="lastSessionSummary">
      {props.datapoints.length > 0 ? (
        <>
          Last session<span className="date">[{sessionDate}]</span>:&nbsp;
          <span className="summary">{sessionSummary}</span>
        </>
      ) : (
        "No data recorded"
      )}
    </span>
  );
}

export default LastSessionSummary;

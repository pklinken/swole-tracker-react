import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { storeService } from "services/store";
import { ExerciseDatapoint } from "models/ExerciseDatapoint";
import { ToastBus } from "./Toasts";

interface DataPointFormProps {
  exerciseId?: number;
  onUpdate: Function;
  datapointId?: number;
  showLabels?: boolean;
}

function DataPointForm(props: DataPointFormProps) {
  const [weight, setWeight] = useState("" as any);
  const [reps, setReps] = useState("" as any);
  const [note, setNote] = useState("");
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [date, setDate] = useState(todaysDateAsYYYYMMDD());
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    if (props.datapointId) {
      const datapoint = storeService.getDatapoint(
        parseInt(props.datapointId as any)
      );
      if (!datapoint) return;

      setWeight(datapoint.weight);
      setReps(datapoint.reps);
      setNote(datapoint.note);
      setDate(new Date(datapoint.date).toISOString().split("T")[0]);
    }
  }, [props.datapointId]);

  function todaysDateAsYYYYMMDD(): string {
    const d = new Date();
    return d.toISOString().split("T")[0];
  }

  function handleFormChange(event: any) {
    const targetName = event?.target?.name;
    const inputValue = event?.target?.value;
    if (targetName === undefined || inputValue === undefined) return;

    switch (targetName) {
      case "weight":
        setWeight(inputValue);
        break;
      case "reps":
        setReps(inputValue);
        break;
      case "note":
        setNote(inputValue);
        break;
      case "date":
        setDate(inputValue);
        break;
      default:
        console.error(
          `handleFormChange with invalid targetName: ${targetName}`
        );
    }
  }

  function validate(): boolean {
    if (
      weight === undefined ||
      isNaN(parseFloat(weight)) ||
      parseFloat(weight) < 0 ||
      reps === undefined ||
      isNaN(parseInt(reps)) ||
      parseInt(reps) <= 0
    ) {
      // Set some classes, will do later when introducing styling framework
      return false;
    } else {
      return true;
    }
  }

  function createNew() {
    storeService.createDatapoint(
      new ExerciseDatapoint(
        -1,
        showDatePicker ? Date.parse(date) : Date.now(),
        props.exerciseId,
        weight,
        reps,
        note
      )
    );

    setWeight("");
    setReps("");
    setNote("");
    ToastBus.publishToast({
      header: "Well done!",
      message: "Your new set has been saved",
    });
  }

  function updateExisting() {
    const datapoint = storeService.getDatapoint(
      parseInt(props.datapointId as any)
    );
    if (!datapoint) return;

    storeService.updateDatapoint(
      new ExerciseDatapoint(
        parseInt(props.datapointId as any),
        showDatePicker ? Date.parse(date) : datapoint.date,
        -1,
        weight,
        reps,
        note
      )
    );

    ToastBus.publishToast({
      header: "Datapoint updated!",
    });
  }

  function handleSubmit(event: any) {
    event?.preventDefault();
    if (!validate()) {
      // showToastt("woah", "you noob");
      setValidated(true);
      return;
    }
    if (props.datapointId) {
      updateExisting();
    } else {
      createNew();
    }

    setValidated(false);
    props.onUpdate();
  }

  return (
    <>
      <Form className="mt-3" noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Group>
          {props.showLabels && <Form.Label>Weight</Form.Label>}
          <Form.Control
            className=""
            type="number"
            placeholder="Weight"
            value={weight}
            name="weight"
            onChange={handleFormChange}
            required
          />
        </Form.Group>
        <Form.Group>
          {props.showLabels && <Form.Label>Reps</Form.Label>}
          <Form.Control
            className=""
            type="number"
            placeholder="Reps"
            value={reps}
            name="reps"
            onChange={handleFormChange}
            required
          />
        </Form.Group>
        <Form.Group>
          {props.showLabels && <Form.Label>Remarks</Form.Label>}
          <Form.Control
            className=""
            type="text"
            placeholder="Remarks.."
            value={note}
            name="note"
            onChange={handleFormChange}
          />
        </Form.Group>
        <Form.Group className="float-right">
          <Form.Check id="dpCheckbox">
            <Form.Check.Input
              type="checkbox"
              onChange={() => setShowDatePicker(!showDatePicker)}
            />
            <Form.Check.Label className="ml-2 mr-2">
              Enable datepicker
            </Form.Check.Label>
          </Form.Check>
        </Form.Group>
        <div className="clearfix"></div>
        {showDatePicker && (
          <Form.Group>
            <Form.Control
              type="date"
              name="date"
              value={date}
              onChange={handleFormChange}
            />
          </Form.Group>
        )}
        <Form.Group className="d-flex flex-row-reverse">
          <Button type="submit" onClick={handleSubmit}>Submit</Button>
        </Form.Group>
      </Form>
    </>
  );
}

export default DataPointForm;

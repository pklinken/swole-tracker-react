import React, { useState, useEffect } from "react";
import {
  IExerciseDataPoint,
  getMostRecentSessionDate,
  getSessionsByDate,
} from "models/ExerciseDatapoint";
import Table from "react-bootstrap/Table";
import { useHistory } from "react-router-dom";

interface Props {
  datapoints: IExerciseDataPoint[];
}

function LastSessionTable(props: Props) {
  const [sessionDate, setSessionDate] = useState("");
  const [latestSessionSets, setLatestSessionSets] = useState(
    [] as IExerciseDataPoint[]
  );
  const history = useHistory();

  useEffect(() => {
    if (props.datapoints.length === 0) return;

    const mostRecentSessionDate = getMostRecentSessionDate(props.datapoints);
    setLatestSessionSets(
      getSessionsByDate(mostRecentSessionDate, props.datapoints)
    );

    setSessionDate(mostRecentSessionDate);
  }, [props]);

  return (
    <>
      {props.datapoints.length > 0 && (
        <>
          <h5>
            Latest set <span className="small text-muted">({sessionDate})</span>
            :
          </h5>
          <Table striped bordered hover size="sm" className="mb-4">
            <thead>
              <tr>
                <th>Set #</th>
                <th>Weight</th>
                <th>Reps</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody>
              {latestSessionSets
                .slice()
                .reverse()
                .map((dp, _i) => (
                  <tr
                    key={_i}
                    onClick={() => history.push(`/datapoint/${dp.id}`)}
                  >
                    <td>{_i + 1}</td>
                    <td>{dp.weight} kg</td>
                    <td>{dp.reps}</td>
                    <td>{dp.note}</td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </>
      )}
    </>
  );
}

export default LastSessionTable;

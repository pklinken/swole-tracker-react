import React from "react";
import DataPointForm from "./DataPointForm";
import { useParams, useHistory } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import { storeService } from "services/store";

function DataPointEdit() {
  const { id } = useParams();
  const history = useHistory();

  function handleDelete() {
    const datapoint = storeService.getDatapoint(parseInt(id));
    if (!datapoint) return;

    storeService.deleteDatapoint(parseInt(id));
    history.push(`/exercise/${datapoint.exerciseId}`);
  }

  function handleBack() {
    const datapoint = storeService.getDatapoint(parseInt(id));
    if (!datapoint) return;

    history.push(`/exercise/${datapoint.exerciseId}`);
  }

  function handleUpdate() {
    const datapoint = storeService.getDatapoint(parseInt(id));
    if (!datapoint) return;

    history.push(`/exercise/${datapoint.exerciseId}`);
  }

  return (
    <>
      <Container fluid className="mt-2">
        <div className="d-flex justify-content-between pt-2">
          <Button variant="light" onClick={handleBack}>
            Back to exercise
          </Button>
          <Button variant="danger" onClick={handleDelete}>
            Delete
          </Button>
        </div>
        <DataPointForm datapointId={id} onUpdate={handleUpdate} showLabels />
      </Container>
    </>
  );
}

export default DataPointEdit;
